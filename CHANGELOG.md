## v0.13.0 (2024-07-26)

### BREAKING CHANGE

-   Methods `spice_is_applied` and `spice_apply` changed signature.

### Feat

-   **update**: autoremove outdated spices

## v0.12.0 (2024-07-17)

### BREAKING CHANGE

-   Freezer version bumped to 3. Re-freeze your buffets.

### Feat

-   **freeze**: auto-remove dangling local patches

### Fix

-   store repeated similar spices in freezer
-   **freeze**: avoid adding extra patch files when remote head is out of sync
-   don't fetch remote head when warming up from freezer
-   **spice-add**: auto-fix spices when using 3-way merge
-   include mrchef patches on autocommits

## v0.11.0 (2024-05-30)

### BREAKING CHANGE

-   `systemLib.${system}` flake output moved to
    `packages.${system}.default.out.passthru.lib`.
-   `packages.${system}.mrchef` moved to `legacyPackages.${system}.mrchef` or
    `packages.${system}.default`.

### Feat

-   **github**: support CLI auth

### Fix

-   **devshell**: create project data dir always
-   make it work on NixOS
-   **spice-rm**: remove `spices` key if empty

### Refactor

-   convert to flake-parts
-   **check**: stream errors, don't consider URL mismatch an error but a warning

## v0.10.0 (2024-05-08)

### Feat

-   **autocommit**: auto-generated fancy commit message when updating

### Fix

-   **testWarmSrc**: adapt to updated nixpkgs version

### Refactor

-   easier way to reflect cold and hot meals

## v0.9.1 (2024-05-03)

### Fix

-   avoid polluting cold status with hot spices
-   detect dirty repo when it's in the middle of an am session

### Perf

-   **nar_hash**: avoid re-hashing the same commit

## v0.9.0 (2024-02-29)

### BREAKING CHANGE

-   `mkPureWarmupHook` can't get `config` and `freezer` args anymore.
-   No derivation asks for `optimizeMealFetch` anymore. It is detected automatically
    with `builtins ? getFlake`.
-   `fetchMeal` arguments changed completely.
-   You need to enable `parse-toml-timestamps` experimental Nix feature.
-   Nix sources are moved to `./mrchef/nix` and included in the distributed Python
    package.
-   only Python 3.10+ is now supported
-   `lib.spice_url_autoconvert()` dropped

### Feat

-   allow requiring NAR hash and store it in the freezer
-   **update**: add `--oldest=N` to update only oldest N meals
-   support patches on files attached on github/gitlab comments
-   allow downloading spices from private github repos
-   **cli**: add `--log-level` switch

### Fix

-   **fetchMeal**: use `nixpkgs.fetchgit` only on http(s) urls
-   **logging**: restore default INFO level for logging
-   better error when branch has no remote

### Refactor

-   **mkPureWarmupHook**: remove alt config/freezer support

## v0.8.1 (2023-10-03)

### Fix

-   **cd**: resurrect pypi autopublish on tags

## v0.8.0 (2023-09-29)

### BREAKING CHANGE

-   `mkWarmSrc` is now called with an attrset of arguments.
-   only x86_64-linux nix packages are provided until
    https://github.com/nix-community/dream2nix/issues/601 is fixed
-   `mrchef.lib.git_is_repo()` is removed; use `git_find_repos.is_git_repo()` instead

### Feat

-   **mkWarmSrc**: allow independent warmup by meal

### Fix

-   **mkWarmSrc**: add a suffix that nix will never understand as a version
-   **mkPureWarmupHook**: don't die on huge freezers
-   find meal to warmup when running from outside the kitchen
-   don't generate patch files on warmup

### Refactor

-   drop poetry2nix and use dream2nix instead
-   faster detection of meal keys by path

### Perf

-   **mkWarmSrc**: faster transformation of freezer
-   **mkWarmSrc**: prefer local builds
-   freeze a lot faster by leveraging local git cache of remote branches when possible
-   **toml**: dramatically faster on non-style-preserving operations
-   **warmup**: skip unselected meals
-   **tomlkit**: bump dependency to get performance improvements
-   remove calls to `lib.git_is_repo()`

## v0.7.0 (2023-07-27)

### BREAKING CHANGE

-   Flake output `lib` is now system-agnostic. System-specific functions moved to
    `systemLib`.

### Feat

-   **nix**: move system-specific lib to systemLib output
-   delete local patch file automatically when removing its spice
-   **nix**: add `mkWarmSrc` function
-   **nix**: `lib.mkSingleMealDrvArgs` for faster builds

### Fix

-   auto-generated patch names now follow a numeric sequence

### Perf

-   faster loading and writing of freezer
-   cache remote heads
-   avoid creating temporary remote
-   **mkWarmSrc**: make faster by disabling unneeded stuff

## v0.6.0 (2023-07-06)

### Feat

-   multiline spices in freezer

### Fix

-   **nix**: do not add mrchef as a propagated build input
-   **nix**: support kitchen subdirs in warmup hook
-   **flake**: prebuilt overlaid package

### Perf

-   fix bottleneck finding git repos in the kitchen

## v0.5.0 (2023-05-17)

### Feat

-   **nix**: warm up only some meals

### Fix

-   allow adding subfolders to kitchen dynamically

## v0.4.0 (2023-04-13)

### BREAKING CHANGE

-   `combinedSource` is removed, as it contained the IFD.

### Feat

-   **nix**: include required binary dependencies
-   **flake**: provide binary caches configurations

### Fix

-   **nix**: remove IFD and add `mkPureWarmupHook`
-   do not leave garbage `_mrchef` remote if fetching it fails
-   **flake**: update precommix and change its input location

## v0.3.0 (2022-06-13)

### Fix

-   let combined source build regardless of meal order
-   optimized github and gitlab fetchers without .git suffix
-   **nix**: don't patch shebangs when building combined source
-   **nix**: allow default fixups if any
-   **nix**: export build git config dir env var properly
-   **nix**: disable git gc globally
-   avoid git background gc

### Perf

-   **nix**: no fixup for combined source
-   **nix**: skip patchelf phase
-   **nix**: prefer local nix builds for source
-   **nix**: avoid duplicating source

### Feat

-   **nix**: smarter optimized meal fetch
-   **nix**: shallow meal fetch by default
-   **nix**: simplify build derivations

### BREAKING CHANGE

-   `mrchefCombinedSource` now gets an `optimizeMealFetch` argument instead of
    `shallow`.
-   `mrchefFetchMeal` now expects an explicit 1st boolean argument to indicate whether
    to use shallow fetching.
-   `fetchCombinedSource` is now named `mrchefCombinedSource`.
-   `mrchefCombinedSource` uses `src` as the drivation source, instead of `mrchefSrc` as
    before.
-   `fetchMeal` is now named `mrchefFetchMeal`.

## v0.2.3 (2022-04-28)

### Fix

-   use \_mrchef origin only privately

## v0.2.2 (2022-04-21)

### Fix

-   add readme

## v0.2.1 (2022-04-21)

### Fix

-   hardcode credentials for pypi and testpypi publishing

## v0.2.0 (2022-04-21)

### Feat

-   publish to test.pypi.org and pypi.org

## v0.1.1 (2022-04-21)

### Fix

-   package published to gitlab registry

## 0.1.0 (2022-04-21)

### Feat

-   MrChef is ready to cook!
-   Works in Python
-   Works in Nix
