{
  lib,
  mrchefLib,
  ...
}:
/*
  *
Given a kitchen and a full subpath, find the meal where that path is contained.

# Example:

```nix
let
  root = ./.;
  target = ./some/nested/meal/sub/folder;
  result = getMealFromPath root target;
in
  assert result == ./some/nested/meal;
```

# Type

```
String -> String -> String
```

# Arguments

root
: Full path to the folder that contains the `mrchef.toml` file.

target
: Full path to the folder that is contained in a meal.
*/
root: target: let
  config = mrchefLib.config root;
  kitchen = builtins.toPath root + "/" + config.kitchen;
in
  lib.findFirst
  (meal: lib.hasPrefix (kitchen + "/${meal}/") "${(builtins.toPath target)}/")
  (throw "No meal found for ${target}")
  (lib.attrNames config.meals)
