"""Module to handle CLI module usage (`python -m mrchef ...`)."""
from .cli import MrChef

MrChef.run()
