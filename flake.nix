rec {
  description = "Metarepo Chef";

  nixConfig = {
    # HACK https://github.com/NixOS/nix/issues/6771
    # TODO Leave only own cache settings when fixed
    extra-trusted-public-keys = [
      "copier.cachix.org-1:sVkdQyyNXrgc53qXPCH9zuS91zpt5eBYcg7JQSmTBG4="
      "devenv.cachix.org-1:w1cLUi8dv3hnoSPGAuibQv+f9TZLr6cv/Hm9XgU50cw="
      "moduon.cachix.org-1:sXMrTN5LuhZyh6CnzYXM4pZkah/Yy//eGKt1oOZc0zw="
      "numtide.cachix.org-1:2ps1kLBUWjxIneOy1Ik6cQjb41X0iXVXeHigGmycPPE="
    ];
    extra-substituters = [
      "https://copier.cachix.org"
      "https://devenv.cachix.org"
      "https://moduon.cachix.org"
      "https://numtide.cachix.org"
    ];
  };

  inputs = {
    flake-compat.url = "github:edolstra/flake-compat/refs/pull/63/head"; # HACK
    flake-parts.url = "github:hercules-ci/flake-parts";
    nix-filter.url = "github:numtide/nix-filter";
    nixpkgs.url = "https://flakehub.com/f/NixOS/nixpkgs/*.tar.gz";
    dream2nix.url = "github:nix-community/dream2nix";
  };

  outputs = inputs: let
    precommix = import ./precommix.nix;
  in
    inputs.flake-parts.lib.mkFlake {inherit inputs;} {
      imports = [precommix.flakeModules.devshell];

      flake.overlays.default = final: prev: rec {
        mrchef = inputs.self.outputs.packages.${final.system}.default;
        mrchefLib = mrchef.out.passthru.lib;
      };

      perSystem = {
        lib,
        pkgs,
        system,
        inputs',
        ...
      }: rec {
        _module.args.pkgs = import inputs.nixpkgs {
          inherit system;
          overlays = [
            inputs.nix-filter.overlays.default
            inputs.self.overlays.default
          ];
        };

        legacyPackages = inputs.dream2nix.lib.importPackages {
          projectRoot = ./.;
          projectRootFile = ".git";
          packagesDir = "mrchef/nix/modules/dream2nix";
          packageSets.nixpkgs = pkgs;
        };

        packages.default = legacyPackages.mrchef;

        devshells.default = {
          devshell.packagesFrom = [packages.default.devShell];

          devshell.startup.python-interpreter.text = ''
            mkdir -p $PRJ_DATA_DIR
            ln -sf $DEVSHELL_DIR/bin $PRJ_DATA_DIR/bin
          '';

          commands = [
            {
              name = "mrchef";
              command = ''
                exec python -m mrchef "$@"
              '';
              help = description;
            }
            {package = packages.default.config.deps.python.pkgs.build;}
            {package = packages.default.config.deps.python.pkgs.twine;}
          ];

          env = [
            {
              name = "PYTHONPATH";
              prefix = "$PRJ_ROOT";
            }
            {
              name = "PATH";
              prefix = packages.default.config.mkDerivation.passthru.binDepsPath;
            }
          ];
        };

        # Test that flake works
        checks = pkgs.callPackages ./tests/nix {};
      };
    };
}
