"""Tests that imitate a human chef working in the kitchen."""

from pathlib import Path
from textwrap import dedent

import pytest
import rtoml
from git_find_repos import is_git_repo
from plumbum import local

from mrchef import lib

from .conftest import SAMPLE_REPOS, cli_run, get_frozen_config, get_user_config


def test_update(tmp_path_factory):
    """Update works as expected."""
    # Create a couple of remote repositories
    remote1 = tmp_path_factory.mktemp("remote1")
    remote1.joinpath("file1").write_text("hello 1")
    git1 = lib.git(remote1)
    git1("init")
    git1("switch", "--create", "branch1")
    git1("add", "-A")
    git1("commit", "--message", "hello 1")
    remote1_commit = lib.git_rev(remote1)
    remote2 = tmp_path_factory.mktemp("remote2")
    remote2.joinpath("file2").write_text("hello 2")
    git2 = lib.git(remote2)
    git2("init")
    git2("switch", "--create", "branch2")
    git2("add", "-A")
    git2("commit", "--message", "hello 2")
    remote2_commit = lib.git_rev(remote2)
    # Create a metarepo where we will include those other repositories
    metarepo = tmp_path_factory.mktemp("metarepo")
    metarepo.joinpath(lib.CONFIG_FILE).write_text(
        dedent(
            f"""\
            version = 1
            kitchen = '.'

            [meals.r1]
            url = '{remote1}'
            branch = 'branch1'

            [meals.r2]
            url = "{remote2}"
            branch = "branch2"
            """
        )
    )
    # Assert they get included properly, both hot and cold
    with pytest.warns(UserWarning, match="Freezer file not found."):
        cli_run("-s", metarepo, "warmup")
    r1, r2 = map(metarepo.joinpath, ("r1", "r2"))
    assert r1.joinpath("file1").read_text() == "hello 1"
    assert r2.joinpath("file2").read_text() == "hello 2"
    with pytest.warns(UserWarning, match="Freezer file not found."):
        cli_run(f"-s{metarepo}", "freeze")
    with local.cwd(metarepo):
        cli_run("check")
    freezer = get_frozen_config(metarepo)
    assert lib.git_rev(r1) == remote1_commit == freezer["meals"]["r1"]["rev"]
    assert lib.git_rev(r2) == remote2_commit == freezer["meals"]["r2"]["rev"]
    # Remote 1 gets updated in the same branch
    remote1.joinpath("file1").write_text("bye 1")
    git1("commit", "--all", "--message", "bye 1")
    remote1_commit = lib.git_rev(remote1)
    # Remote 2 gets updated in a separate branch
    git2("switch", "--create", "feature-branch2")
    remote2.joinpath("file2").write_text("bye 2")
    git2("commit", "--all", "--message", "bye 2")
    # Assert metarepo gets properly updated
    cli_run("--starting-folder", metarepo, "update")
    assert r1.joinpath("file1").read_text() == "bye 1"
    assert r2.joinpath("file2").read_text() == "hello 2"
    cli_run(f"--starting-folder={metarepo}", "check")
    freezer = get_frozen_config(metarepo)
    assert lib.git_rev(r1) == remote1_commit == freezer["meals"]["r1"]["rev"]
    assert lib.git_rev(r2) == remote2_commit == freezer["meals"]["r2"]["rev"]
    # Cook local patch in r2
    r2.joinpath("local-patch").write_text("doing local stuff")
    lib.git(r2)("add", "-A")
    lib.git(r2)("commit", "--message", "local patch")
    # Store local patch in freezer
    cli_run("-s", metarepo, "freeze")
    user_config = get_user_config(metarepo)
    assert user_config["meals"]["r2"]["spices"] == ["mrchef-000.patch"]
    freezer = get_frozen_config(metarepo)
    assert lib.git_rev(r2) != remote2_commit
    assert freezer["meals"]["r2"]["rev"] == remote2_commit
    assert "mrchef-000.patch" in freezer["spices"]
    assert (
        metarepo.joinpath("mrchef-000.patch").read_text()
        == freezer["spices"]["mrchef-000.patch"]
    )


@pytest.mark.impure
def test_init_meal_add_spice_add_meal_rm(tmp_path):
    """Init a kitchen, add meal, spice it up, remove it."""
    initial_user_config = {"version": 1, "kitchen": "kitchen", "require_nar_hash": True}
    fork = tmp_path / "kitchen" / "autopretty"
    url = "https://github.com/copier-org/autopretty.git"
    branch = "main"
    rev = "56d43ec053fb1969a2450c569d6b8127531942d3"
    spice_url = "https://github.com/copier-org/autopretty/commit/d46b05b4cff321425e97cc4a04bd1950332a898b.patch"
    with local.cwd(tmp_path):
        # Init kitchen
        cli_run("init")
        user_config = get_user_config(tmp_path)
        assert user_config == initial_user_config
        # Add new meal
        cli_run("meal-add", fork, url, branch, rev)
        assert is_git_repo(fork)
        assert lib.git_rev(fork) == rev
        user_config = get_user_config(tmp_path)
        assert user_config == {
            "version": lib.CONFIG_VERSION,
            "kitchen": "kitchen",
            "require_nar_hash": True,
            "meals": {
                "autopretty": {
                    "url": url,
                    "branch": branch,
                    "rev": rev,
                }
            },
        }
        freezer = get_frozen_config(tmp_path)
        assert freezer == {
            "version": lib.FREEZER_VERSION,
            "meals": {
                "autopretty": {
                    "last_update": lib.now(),
                    "rev": rev,
                    "nar_hash": "sha256-Lgq63spHq5b6iuscZNsGBSWGhRCQnYoknikqSxIZ9zE=",
                }
            },
            "spices": {},
        }
        cli_run("check")
        # Spice up meal
        cli_run("spice-add", fork, spice_url)
        user_config = get_user_config(tmp_path)
        assert user_config == {
            "version": lib.CONFIG_VERSION,
            "kitchen": "kitchen",
            "require_nar_hash": True,
            "meals": {
                "autopretty": {
                    "url": url,
                    "branch": branch,
                    "rev": rev,
                    "spices": [spice_url],
                }
            },
        }
        freezer = get_frozen_config(tmp_path)
        assert freezer["meals"]["autopretty"]["rev"] == rev
        assert spice_url in freezer["spices"]
        # Remove meal
        cli_run("meal-rm", fork)
        assert not fork.exists()
        user_config = get_user_config(tmp_path)
        assert "autopretty" not in user_config.get("meals", {})
        freezer = get_frozen_config(tmp_path)
        assert freezer == {
            "meals": {},
            "spices": {},
            "version": lib.FREEZER_VERSION,
        }


def test_meal_updated_elsewhere_frozen_here(tmp_path):
    # Create a fork of 123.bundle that is outdated by 1 commit
    fork = tmp_path / "123-fork"
    fork.mkdir()
    git1 = lib.git(fork)
    git1("clone", "--bare", SAMPLE_REPOS / "123.bundle", fork)
    git1("update-ref", "refs/heads/main", "a6cf33141a9f770967e3c132e0aa0071c3db2d43")
    # Create a kitchen where we include the fork
    kitchen = tmp_path / "kitchen"
    kitchen.mkdir()
    kitchen.joinpath(lib.CONFIG_FILE).write_text(
        dedent(
            """\
            version = 1
            kitchen = '.'
            """
        )
    )
    cli_run("-s", kitchen, "meal-add", kitchen / "123", fork, "main")
    cli_run("-s", kitchen, "warmup")
    cli_run(f"-s{kitchen}", "freeze")
    # Assert the freezer is correct and no local patches exist
    freezer = get_frozen_config(kitchen)
    assert freezer == {
        "meals": {
            "123": {
                "last_update": lib.now(),
                "rev": "a6cf33141a9f770967e3c132e0aa0071c3db2d43",
            }
        },
        "spices": {},
        "version": lib.FREEZER_VERSION,
    }
    assert not list(kitchen.glob("*.patch"))
    # Update the fork
    git1("update-ref", "refs/heads/main", "e757a53dccbb5407994f03dc419651275dc6eacf")
    # Imagine that somebody else updates your kitchen from the updated fork,
    # and you pull those changes
    freezer["meals"]["123"] = {"rev": "e757a53dccbb5407994f03dc419651275dc6eacf"}
    rtoml.dump(freezer, kitchen / lib.FREEZER_FILE, pretty=True)
    # Now you obviously want to warmup again
    cli_run("-s", kitchen, "warmup")
    # Re-freezing should make no difference
    cli_run(f"-s{kitchen}", "freeze")
    # Assert the freezer is correct and no local patches exist
    freezer = get_frozen_config(kitchen)
    assert freezer == {
        "meals": {
            "123": {
                "last_update": lib.now(),
                "rev": "e757a53dccbb5407994f03dc419651275dc6eacf",
            }
        },
        "spices": {},
        "version": lib.FREEZER_VERSION,
    }
    assert not list(kitchen.glob("*.patch"))


def test_updating_removes_unneeded_spices(tmp_path: Path):
    # Meal one is outdated
    tmp_path.joinpath(lib.CONFIG_FILE).write_text(
        f"""
        version = 1
        kitchen = "code"

        [meals.one]
        url = "{SAMPLE_REPOS / '123.bundle'}"
        branch = "main"
        """
    )
    tmp_path.joinpath(lib.FREEZER_FILE).write_text(
        f"""
        version = {lib.FREEZER_VERSION}
        [meals.one]
        last_update = 1776-07-04T08:47:00Z
        rev = "b72639b17d5882c2ce1448f7ed452e596dbe6acd"
        """
    )
    lib.Worker(tmp_path).warmup()
    # We apply a local modification
    one = tmp_path / "code" / "one"
    one.joinpath("file-2.txt").write_text("2\n")
    lib.git(one)("add", "-A")
    lib.git(one)("commit", "-am", "Local change")
    # We freeze the local modification
    lib.Worker(tmp_path).freeze()
    assert get_user_config(tmp_path)["meals"]["one"]["spices"] == [
        "code/mrchef-000.patch"
    ]
    assert tmp_path.joinpath("code", "mrchef-000.patch").exists()
    assert "code/mrchef-000.patch" in get_frozen_config(tmp_path)["spices"]
    # Now that change lands upstream, so we just update
    lib.Worker(tmp_path).update()
    # The local patch should have been removed
    assert not tmp_path.joinpath("code", "mrchef-000.patch").exists()
    assert not get_frozen_config(tmp_path)["spices"]
    assert "spices" not in get_user_config(tmp_path)["meals"]["one"]
    # The freezer should have been updated
    assert (
        get_frozen_config(tmp_path)["meals"]["one"]["rev"]
        == "e757a53dccbb5407994f03dc419651275dc6eacf"
    )


def test_warmup_after_dirty_changes(tmp_path: Path):
    with local.cwd(tmp_path):
        # Init with one meal
        cli_run("init")
        # Disable NAR hashing
        Path(lib.CONFIG_FILE).write_text(
            Path(lib.CONFIG_FILE).read_text().replace("require_nar_hash = true", "")
        )
        meal = tmp_path / "kitchen" / "123"
        cli_run("meal-add", meal, SAMPLE_REPOS / "123.bundle", "main")
        # Make a local change in a local branch
        lib.git(meal)("switch", "--create", "feature")
        meal.joinpath("change").write_text("local change")
        lib.git(meal)("add", "-A")
        lib.git(meal)("commit", "-am", "Local change")
        # Warmup should work
        cli_run("warmup")
        # Meal branch is main again
        assert lib.git(meal)("rev-parse", "--abbrev-ref", "HEAD").strip() == "main"
        assert lib.git_rev(meal) == "e757a53dccbb5407994f03dc419651275dc6eacf"
