"""Common configurations and helpers for test suite."""

from pathlib import Path
from shutil import copytree, which

import pytest
import rtoml
from plumbum import local

from mrchef import cli, lib

PROJECT_ROOT = Path(__file__).parent.parent
SAMPLE_REPOS = PROJECT_ROOT / "tests" / "sample_repos"


@pytest.fixture(autouse=True)
def testing_env(monkeypatch):
    """Patch tests environment."""
    extra_env = {
        # Git basic configuration
        "EMAIL": "mrchef@example.com",
        # Find the appropriate git binary
        "GIT_AUTOSHARE_GIT_BIN": which("git"),
        # We use a lot autopretty to test; autosharing makes all faster
        "GIT_AUTOSHARE_CONFIG_DIR": str(Path(__file__).parent / "git-autoshare-config"),
        # Let errors raise, for easier debugging
        "MRCHEF_ERROR_SLURP": "0",
        # Mock date to freeze tests
        "MRCHEF_MOCK_DATE": "1985-10-26T01:21:00Z",
        # Forget GPG
        # DOCS https://git-scm.com/docs/git-config#Documentation/git-config.txt-GITCONFIGCOUNT
        "GIT_CONFIG_COUNT": "1",
        "GIT_CONFIG_KEY_0": "commit.gpgsign",
        "GIT_CONFIG_VALUE_0": "false",
    }
    # Patch current python process
    for key, value in extra_env.items():
        monkeypatch.setenv(key, value)
    # Patch subprocesses
    with local.env(**extra_env):
        yield


@pytest.fixture
def assets_orig(request) -> Path:
    """Get path for current module tests folder.

    Each pytest module can have a folder with the same name as the module. It
    contains sample data files used to run the tests in the module.

    This fixture helps getting to that path quickly. It points to the source
    path, not to a temporary copy.
    """
    module_file = Path(request.module.__file__)
    return module_file.parent / module_file.stem


@pytest.fixture
def assets_copy(tmp_path_factory, assets_orig) -> Path:
    """Copy test directory before starting.

    Searches for a directory in the tests folder with the same name as the module
    and copies it to a temporary destination.
    """
    tmp_dir = tmp_path_factory.mktemp(assets_orig.name)
    copytree(
        assets_orig,
        tmp_dir,
        dirs_exist_ok=True,
    )
    yield tmp_dir


@pytest.fixture
def assets_copy_repo(assets_copy) -> Path:
    """Copy test directory before starting and convert it to a git repo."""
    _git = lib.git(assets_copy)
    _git("init")
    _git("add", "-A")
    _git("commit", "-m", "Initial commit")
    yield assets_copy


def cli_run(*args):
    """Shortcut to issue CLI calls."""
    instance, return_code = cli.MrChef.run(["mrchef", *map(str, args)], exit=False)
    # Usually, after exiting CLI, cache is cleared, so let's imitate that
    lib.git_remote_head.cache_clear()
    assert return_code == 0


def get_user_config(path: Path):
    """Get user config starting from path."""
    return rtoml.load(path.joinpath(lib.CONFIG_FILE).open())


def get_frozen_config(path: Path):
    """Get frozen config starting from path."""
    return rtoml.load(path.joinpath(lib.FREEZER_FILE).open())
