from pathlib import Path

import pytest

from mrchef import lib

from .conftest import SAMPLE_REPOS, get_frozen_config, get_user_config

BUNDLE123 = SAMPLE_REPOS / "123.bundle"


def test_warmup(tmp_path):
    """Generate a kitchen and warm it up."""
    tmp_path.joinpath(lib.CONFIG_FILE).write_text(
        f"""
        version = {lib.CONFIG_VERSION}
        kitchen = "."

        [meals.one]
        url = "{BUNDLE123}"
        branch = "main"

        [meals.two]
        url = "{BUNDLE123}"
        branch = "main"

        [meals.three]
        url = "{BUNDLE123}"
        branch = "main"
        """
    )
    tmp_path.joinpath(lib.FREEZER_FILE).write_text(
        f"""
        version = {lib.FREEZER_VERSION}
        [meals.one]
        last_update = 1776-07-04T08:47:00Z
        rev = "b72639b17d5882c2ce1448f7ed452e596dbe6acd"
        [meals.two]
        last_update = 1936-05-25T07:50:00Z
        rev = "a6cf33141a9f770967e3c132e0aa0071c3db2d43"
        [meals.three]
        last_update = 1985-10-26T01:20:00Z
        rev = "e757a53dccbb5407994f03dc419651275dc6eacf"
        """
    )
    lib.Worker(tmp_path).warmup()
    assert tmp_path.joinpath("one", "file-1.txt").read_text() == "1\n"
    assert not tmp_path.joinpath("one", "file-2.txt").exists()
    assert not tmp_path.joinpath("one", "file-3.txt").exists()
    assert tmp_path.joinpath("two", "file-1.txt").read_text() == "1\n"
    assert tmp_path.joinpath("two", "file-2.txt").read_text() == "2\n"
    assert not tmp_path.joinpath("two", "file-3.txt").exists()
    assert tmp_path.joinpath("three", "file-1.txt").read_text() == "1\n"
    assert tmp_path.joinpath("three", "file-2.txt").read_text() == "2\n"
    assert tmp_path.joinpath("three", "file-3.txt").read_text() == "3\n"


def test_double_warmup_discards_changes(tmp_path: Path):
    tmp_path.joinpath(lib.CONFIG_FILE).write_text(
        f"""
        version = 1
        kitchen = "code"

        [meals.one]
        url = "{BUNDLE123}"
        branch = "main"
        """
    )
    tmp_path.joinpath(lib.FREEZER_FILE).write_text(
        """
        version = 1
        [meals.one]
        rev = "b72639b17d5882c2ce1448f7ed452e596dbe6acd"
        """
    )
    code = tmp_path / "code"
    one = code / "one"
    thing = one / "thing"
    nothing = one / "nothing"
    # Warmup
    lib.Worker(tmp_path).warmup()
    # Manual edits
    thing.write_text("stuff")
    _git = lib.git(one)
    _git("add", "thing")
    _git("commit", "-m", "Thing & stuff")
    nothing.write_text("emptiness")
    _git("add", "nothing")
    _git("commit", "-m", "Nothing & emptiness")
    # Warmup again
    lib.Worker(tmp_path).warmup()
    # Things are frozen
    assert one.joinpath("file-1.txt").read_text() == "1\n"
    assert not one.joinpath("file-2.txt").exists()
    assert not one.joinpath("file-3.txt").exists()
    assert not thing.exists()
    assert not nothing.exists()
    assert not list(code.glob("*.patch"))


@pytest.mark.parametrize("updated_meal", ("one", "two", "three"))
def test_single_meal_update(tmp_path, updated_meal: str):
    test_warmup(tmp_path)
    expected = lib.Worker(tmp_path).config_frozen
    lib.Worker(tmp_path).update(tmp_path / updated_meal)
    # It is expected that the updated meal contains the same rev as meal "three"
    expected["meals"][updated_meal]["rev"] = "e757a53dccbb5407994f03dc419651275dc6eacf"
    # It is expected that the updated meal has a new last_update date
    expected["meals"][updated_meal]["last_update"] = lib.now()
    assert expected == lib.Worker(tmp_path).config_frozen


@pytest.mark.parametrize("oldest", (0, 1, 2, 3))
def test_oldest_meal_update(tmp_path, oldest: int):
    test_warmup(tmp_path)
    lib.Worker(tmp_path).update(oldest=oldest)
    must_update = [
        {"one", "two", "three"},
        {"one"},
        {"one", "two"},
        {"one", "two", "three"},
    ][oldest]
    must_stay = {"one", "two", "three"} - must_update
    freezer = lib.Worker(tmp_path).config_frozen
    for meal in must_update:
        assert freezer["meals"][meal]["last_update"] == lib.now()
        assert (
            freezer["meals"][meal]["rev"] == "e757a53dccbb5407994f03dc419651275dc6eacf"
        )
    for meal in must_stay:
        assert freezer["meals"][meal]["last_update"] < lib.now()
        if meal != "three":
            assert (
                freezer["meals"][meal]["rev"]
                != "e757a53dccbb5407994f03dc419651275dc6eacf"
            )


def test_subfolder_add(tmp_path: Path):
    tmp_path.joinpath(lib.CONFIG_FILE).write_text(
        f"""
        version = 1
        kitchen = "kitchen"

        [meals."1/123"]
        url = "{BUNDLE123}"
        branch = "main"
        """
    )
    tmp_path.joinpath(lib.FREEZER_FILE).write_text(
        """
        version = 1
        [meals."1/123"]
        rev = "b72639b17d5882c2ce1448f7ed452e596dbe6acd"
        """
    )
    lib.Worker(tmp_path).warmup()
    lib.Worker(tmp_path).meal_add(
        tmp_path / "kitchen" / "2" / "123", str(BUNDLE123), "main"
    )
    worker = lib.Worker(tmp_path)
    assert worker.config_user["meals"]["1/123"]["url"] == str(BUNDLE123)
    assert worker.config_user["meals"]["1/123"]["branch"] == "main"
    assert worker.config_user["meals"]["2/123"]["url"] == str(BUNDLE123)
    assert worker.config_user["meals"]["2/123"]["branch"] == "main"
    assert (
        worker.config_frozen["meals"]["1/123"]["rev"]
        == "b72639b17d5882c2ce1448f7ed452e596dbe6acd"
    )
    assert (
        worker.config_frozen["meals"]["2/123"]["rev"]
        == "e757a53dccbb5407994f03dc419651275dc6eacf"
    )


def test_similar_patches(tmp_path: Path):
    tmp_path.joinpath(lib.CONFIG_FILE).write_text(
        f"""
        version = 1
        kitchen = "."

        [meals.one]
        url = "{BUNDLE123}"
        branch = "main"

        [meals.two]
        url = "{BUNDLE123}"
        branch = "main"
        """
    )
    tmp_path.joinpath(lib.FREEZER_FILE).write_text(
        f"""
        version = {lib.FREEZER_VERSION}
        [meals.one]
        last_update = 1776-07-04T08:47:00Z
        rev = "b72639b17d5882c2ce1448f7ed452e596dbe6acd"
        [meals.two]
        last_update = 1936-05-25T07:50:00Z
        rev = "a6cf33141a9f770967e3c132e0aa0071c3db2d43"
        """
    )
    lib.Worker(tmp_path).warmup()
    # Make the same change in both meals
    with tmp_path.joinpath("one", "file-1.txt").open("a") as fd:
        fd.write("patched\n")
    with tmp_path.joinpath("two", "file-1.txt").open("a") as fd:
        fd.write("patched\n")
    # Commit the changes
    lib.git(tmp_path / "one")("commit", "-am", "Patched")
    lib.git(tmp_path / "two")("commit", "-am", "Patched")
    # Freeze the changes
    lib.Worker(tmp_path).freeze()
    # Check that both patches are created and frozen
    assert tmp_path.joinpath("mrchef-000.patch").exists()
    assert tmp_path.joinpath("mrchef-001.patch").exists()
    user1 = get_user_config(tmp_path)
    # This is a weird way to check that each meal gets one patch, but the order
    # is not deterministic
    valid_spices = [["mrchef-000.patch"], ["mrchef-001.patch"]]
    valid_spices.remove(user1["meals"]["one"]["spices"])
    valid_spices.remove(user1["meals"]["two"]["spices"])
    assert not valid_spices
    freezer1 = get_frozen_config(tmp_path)
    # Both spices have the same ID, but are for different meals, so they are
    # stored separately
    assert (
        lib.git_get_patch_ids_map(freezer1["spices"]["mrchef-000.patch"]).keys()
        == lib.git_get_patch_ids_map(freezer1["spices"]["mrchef-001.patch"]).keys()
    )
    assert (
        freezer1["spices"]["mrchef-000.patch"] != freezer1["spices"]["mrchef-001.patch"]
    )
    # Freezing again changes nothing
    lib.Worker(tmp_path).freeze()
    user2 = get_user_config(tmp_path)
    assert user1 == user2
    freezer2 = get_frozen_config(tmp_path)
    assert freezer1 == freezer2


def test_warmup_editing_config_manually(tmp_path: Path):
    # User creates a config
    tmp_path.joinpath(lib.CONFIG_FILE).write_text(
        f"""
        version = 1
        kitchen = "code"

        [meals.one]
        url = "{BUNDLE123}"
        branch = "main"
        """
    )
    lib.Worker(tmp_path).warmup()
    lib.Worker(tmp_path).freeze()
    assert tmp_path.joinpath("code", "one", "file-3.txt").read_text() == "3\n"
    assert "one" in get_frozen_config(tmp_path)["meals"]
    # User adds meal two manually
    tmp_path.joinpath(lib.CONFIG_FILE).write_text(
        f"""
        version = 1
        kitchen = "code"

        [meals.one]
        url = "{BUNDLE123}"
        branch = "main"

        [meals.two]
        url = "{BUNDLE123}"
        branch = "main"
        """
    )
    lib.Worker(tmp_path).warmup()
    lib.Worker(tmp_path).freeze()
    assert tmp_path.joinpath("code", "two", "file-3.txt").read_text() == "3\n"
    assert tmp_path.joinpath(lib.FREEZER_FILE).exists()
    assert "two" in get_frozen_config(tmp_path)["meals"]
