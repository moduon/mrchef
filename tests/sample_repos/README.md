# Sample repos

These repos are here for testing purposes.

By having full repos within this repo, we can do pure tests that do not depend on things
such as network connectivity.

The sample repos are in `*.bundle` files, generated with
[`git bundle` command](https://git-scm.com/docs/git-bundle).

You can just clone them and treat them like normal git repos.

Current bundles:

## `123.bundle`

```shell
➤ git log --graph --decorate
* commit e757a53dccbb5407994f03dc419651275dc6eacf (HEAD -> main, origin/main)
| Author: Jairo Llopis <yajo.sk8@gmail.com>
| Date:   Wed Feb 8 10:49:24 2023 +0000
|
|     add file 3
|
* commit a6cf33141a9f770967e3c132e0aa0071c3db2d43
| Author: Jairo Llopis <yajo.sk8@gmail.com>
| Date:   Wed Feb 8 10:49:11 2023 +0000
|
|     add file 2
|
* commit b72639b17d5882c2ce1448f7ed452e596dbe6acd
  Author: Jairo Llopis <yajo.sk8@gmail.com>
  Date:   Wed Feb 8 10:48:37 2023 +0000

      add file 1

➤ tree
.
├── file-1.txt
├── file-2.txt
└── file-3.txt

1 directory, 3 files
```

All three files' content are the respective numbers.
