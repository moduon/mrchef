# This test should be kept short to help readers understand main nix usage.
{
  mrchefLib,
  nix-filter,
  python3,
  stdenv,
  ...
}: let
  # This is your metarepo source. You can inspect the folder. You'll see it's
  # a small example of some hello-world implementations. It includes a few patches.
  # See the `checkPhase` below to know what to expect from here.
  src = nix-filter.filter {
    root = ./sample_full;
    # I'm filtering it to reduce rebuilds to the minimum. Typically, a metarepo
    # only needs the files involved in mrchef warmup. The rest of the sources
    # will be obtained from there using the wamrup hook.
    include = [".mrchef.freezer.toml" "kitchen" "mrchef.toml"];
  };

  # Calling this function will create a build hook that will call `mrchef warmup`
  # during your derivation's `postUnpack` phase. The warmup will be mocked to
  # get pure inputs from the nix store, so we don't need an IFD or FOD to let
  # Mr. Chef do his job. We must pass the same `src` as the one used for the
  # final derivation below.
  pureWarmupHook = mrchefLib.mkPureWarmupHook {inherit src;};
in
  stdenv.mkDerivation {
    inherit src;
    name = "testMinimal";

    # You can configure your build, patch, install, etc. phases at will.
    # This is just a minimal example to let the derivation build successfully.
    installPhase = ''
      touch $out
    '';

    # We need the built hook as a build input to let Mr. Chef do his job
    buildInputs = [pureWarmupHook];

    # Assertions to make sure the example works
    checkInputs = [python3];
    doCheck = true;
    checkPhase = ''
      (
        set -x

        # Dirs exist
        test -d "kitchen/autopretty"
        test -d "kitchen/hello-world ssh"
        test -d "kitchen/octocat/hello-world"
        test -d "kitchen/pypi_hello_world"

        # Spices are applied
        grep ",tf," "kitchen/autopretty/.editorconfig"
        test "Hello World!" == "$(cat "kitchen/hello-world ssh/README")"
        test "Hello World!" == "$(cat "kitchen/octocat/hello-world/README")"
        test "Hello World!" == "$(cat "kitchen/pypi_hello_world/README")"

        # New file that comes from local patch exists
        test "hello" == "$(cat kitchen/octocat/hello-world/newfile)"

        # No meals have .git directory
        test ! -e "kitchen/hello-world ssh/.git"
        test ! -e "kitchen/octocat/hello-world/.git"
        test ! -e "kitchen/pypi_hello_world/.git"

        # Patched mrchef.toml has no /nix/store routes
        test 0 -eq "$(grep --count /nix/store mrchef.toml)"

        # Python can import the module code, and it was properly patched
        test "Hello Mr. Chef!!!" == "$(
          cd kitchen/pypi_hello_world
          python -c 'import pypi_hello_world; pypi_hello_world.hello()'
        )"
      )
    '';
  }
