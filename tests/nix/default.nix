# Helper to generate an attrset from the other nix files in this folder
{
  callPackage,
  lib,
  ...
}: let
  testFiles =
    lib.filterAttrs
    (name: value: value == "regular" && name != "default.nix" && lib.hasSuffix ".nix" name)
    (builtins.readDir ./.);
in
  lib.mapAttrs'
  (name: value:
    lib.nameValuePair (lib.removeSuffix ".nix" name)
    ((callPackage (./. + "/${name}") {}).overrideAttrs (old: {
      pname = "test-${name}";
    })))
  testFiles
