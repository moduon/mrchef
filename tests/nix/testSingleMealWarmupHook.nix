# In this test we build a package for a single meal, using mkPureWarmupHook
{
  mrchefLib,
  nix-filter,
  python3Packages,
  ...
}: let
  src = nix-filter.filter {
    root = ./sample_full;
    include = [".mrchef.freezer.toml" "kitchen" "mrchef.toml"];
  };
  pureWarmupHook = mrchefLib.mkPureWarmupHook {inherit src;};
in
  python3Packages.buildPythonPackage rec {
    inherit src;
    version = "1.0";
    pname = "pypi_hello_world";
    buildInputs = [pureWarmupHook];

    # We will only use one meal, so we warm up only that one, to speed up the build
    mealsToWarmup = builtins.toJSON [pname];

    # This phase runs after unpackPhase, so the kitchen is warm already; we just
    # enter the meal directory to let the rest of the derivation continue using
    # only that source
    patchPhase = ''
      cd kitchen/pypi_hello_world
    '';

    # The python package should build properly
    doCheck = true;
    checkPhase = ''
      (
        set -x

        # Dirs exist
        test ! -d "../hello-world ssh"
        test ! -d "../hello-world"
        test -d "../pypi_hello_world"

        # Readme was patched
        test "Hello World!" == "$(cat "README")"

        # No .git directory
        test ! -e ".git"

        # Patched mrchef.toml has no /nix/store routes
        test 0 -eq "$(grep --count /nix/store ../../mrchef.toml)"

        # Python can import the module code, and it was properly patched
        test "Hello Mr. Chef!!!" == "$(
          python -c 'import pypi_hello_world; pypi_hello_world.hello()'
        )"
      )
    '';
  }
