# In this test we build warmed-up sources for a single meal
{
  mrchefLib,
  nix-filter,
  runCommand,
  lib,
  ...
}:
runCommand "testSingleMealWarmSrc" {
  src = mrchefLib.mkWarmSrc {
    src = ./sample_full;
    mealsToWarmup = ["octocat/hello-world"];
  };
} ''
  (
    set -ex

    cd $src

    # Main project is there
    test -f pyproject.toml

    # The meal is appropriately warmed up
    test -d kitchen/octocat/hello-world
    test hello == "$(cat kitchen/octocat/hello-world/newfile)"

    # Other meals don't exist
    test ! -d kitchen/pypi_hello_world
    test ! -d "kitchen/hello-world ssh"

    # Success
    touch $out
  )
''
