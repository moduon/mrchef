# Test warmed-up sources
{
  mrchefLib,
  python3,
  runCommandLocal,
  ...
}: let
  coldSrc = ./sample_full;
  src = mrchefLib.mkWarmSrc {src = coldSrc;};
in
  runCommandLocal "testWarmSrc" {
    inherit src;
    nativeBuildInputs = [python3];
    PYTHONPATH = "${src}/kitchen/pypi_hello_world";
  } ''
    (
      set -x
      cd $src

      # Python can import the module code, and it was properly patched
      test "Hello Mr. Chef!!!" == "$(python -c 'import sample_full')"

      touch $out
    )
  ''
