# Make sure we can find the meal from a subpath
{
  lib,
  mrchefLib,
  nix-filter,
  runCommandLocal,
  ...
}: let
  src = nix-filter.filter {
    root = ./sample_full;
    include = [".mrchef.freezer.toml" "kitchen" "mrchef.toml"];
  };
  fn = mrchefLib.getMealFromPath src;
  failures = lib.runTests {
    testPypiABC = {
      expr = fn (src + "/kitchen/pypi_hello_world/a/b/c");
      expected = "pypi_hello_world";
    };

    testOctocatABC = {
      expr = fn (src + "/kitchen/octocat/hello-world/a/b/c");
      expected = "octocat/hello-world";
    };

    testSshABC = {
      expr = fn (src + "/kitchen/hello-world ssh/a/b/c");
      expected = "hello-world ssh";
    };

    testDoomed = {
      expr = fn (src + "/kitchen/../kitchen/pypi_hello_world/a/..//../hello-world ssh/a/b/c");
      expected = "hello-world ssh";
    };

    testSeemsLegitButNo = {
      expr = builtins.tryEval (fn (src + "/kitchen/pypi_hello_world_and_more/a/b/c"));
      expected = {
        success = false;
        value = false;
      };
    };
  };
in
  runCommandLocal "testGetMealFromPath" {failures = builtins.toJSON failures;} ''
    (
      set -x
      test "$failures" = "[]"
    )
    touch $out
  ''
