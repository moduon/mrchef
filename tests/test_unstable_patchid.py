"""Tests related to adding a meal ."""
from textwrap import dedent

import pytest
from plumbum import local

from mrchef import lib

from .conftest import get_frozen_config


@pytest.mark.impure
def test_spice_add_fails(assets_copy):
    worker = lib.Worker(assets_copy)
    worker.warmup()
    # This spice fails to apply cleanly because it produces merge conflicts
    patch_name = "pr-2957.patch"
    with pytest.raises(lib.MrChefError, match="Failed to apply spice"):
        worker.spice_add(assets_copy / "code" / "server-tools", patch_name)


@pytest.mark.impure
def test_spice_add_3way_merge(assets_copy, caplog):
    # This adds the same spice that fails to apply in `test_spice_add_fails()`
    # above. However, this time we have the full history of all branches in the
    # repo (thanks to git-autoshare), and Git is able to do a 3-way merge.
    # Thus, the patch will be applied, but Git will modify it automatically.
    worker = lib.Worker(assets_copy)
    with local.env(GIT_AUTOSHARE_CONFIG_DIR=assets_copy):
        worker.warmup()
    caplog.clear()
    patch_name = "pr-2957.patch"
    worker.spice_add(assets_copy / "code" / "server-tools", patch_name)
    assert caplog.record_tuples == [
        (
            "mrchef",
            30,
            "Spice pr-2957.patch was not applied cleanly; "
            "storing local version with modifications",
        ),
        ("mrchef", 20, "Freezing..."),
        ("mrchef", 20, "Spice was not frozen, downloading: code/mrchef-000.patch"),
    ]
    # Original patch added as comment; real patch is auto-generated
    expected_config = dedent(
        """\
        version = 1
        kitchen = "code"

        [meals.server-tools]
        url = "https://github.com/OCA/server-tools"
        branch = "15.0"
        spices = [
            # Locally modified pr-2957.patch
            "code/mrchef-000.patch",
        ]
        """
    )
    assert worker.config_file.read_text() == expected_config
    freezer = get_frozen_config(assets_copy)
    assert (
        freezer["spices"]["code/mrchef-000.patch"]
        == assets_copy.joinpath("code", "mrchef-000.patch").read_text()
    )
