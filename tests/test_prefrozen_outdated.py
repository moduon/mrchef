from logging import ERROR, WARNING

import pytest

from mrchef import lib

from .conftest import get_frozen_config


@pytest.mark.impure
def test_warmup_check_freeze(caplog, assets_copy, assets_orig):
    lib.Worker(assets_copy).warmup()
    caplog.clear()
    with pytest.raises(lib.MrChefError):
        lib.Worker(assets_copy).check()
    assert caplog.record_tuples == [
        (
            "mrchef",
            WARNING,
            f"{assets_copy / lib.FREEZER_FILE} has version 1 instead of {lib.FREEZER_VERSION}; re-freeze or bad things could happen",
        ),
        ("mrchef", 20, "Spice was not frozen, downloading: ./000-local.patch"),
        ("mrchef", ERROR, "Freezer is outdated."),
    ]
    # Nothing changed, so frozen file should stay the same
    new_freezer = get_frozen_config(assets_copy)
    old_freezer = get_frozen_config(assets_orig)
    assert new_freezer == old_freezer
    # Update freezer, check should pass this time
    lib.Worker(assets_copy).freeze()
    lib.Worker(assets_copy).check()
